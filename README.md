# Iris in Flask

Basic model to show how CI/CD pipelines can be used

Example call

```
curl -X POST --data '{"values":[5.1, 3.5, 1.4, 0.2]}' http://0.0.0.0:5000/score

curl -X POST --data '{"values":[5.7, 2.8, 4.5, 1.3]}' http://0.0.0.0:5000/score
```

or

```
curl -X POST --data '{"values":[5.7, 2.8, 4.5, 1.3]}' https://irisincicd.mybluemix.net/score

curl -X POST --data '{"values":[5.1, 3.5, 1.4, 0.2]}' https://irisincicd.mybluemix.net/score
```

# Tasks

Once could retrain with tasks

```
https://github.com/ihuston/python-cf-tasks
```

## CF basic commands
```
cf login --sso
```

with
```
api.us-south.cf.cloud.ibm.com
```


```
cf apps

cf run-task irisincicd "python knn_train.py"

cf tasks irisincicd
cf logs irisincicd

```



