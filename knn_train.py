from sklearn.datasets import load_iris
from sklearn.neighbors import KNeighborsClassifier
from sklearn import metrics

import pickle

iris = load_iris()

X = iris.data
y = iris.target


knn = KNeighborsClassifier(n_neighbors=10)

knn.fit(X, y)

y_pred = knn.predict(X)
print(metrics.accuracy_score(y, y_pred))

pickle.dump( knn, open( "knn.pkl", "wb" ) )

knn = pickle.load( open( "knn.pkl", "rb" ) )

