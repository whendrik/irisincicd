import os
import json
import requests
import pickle
import numpy as np

from flask import Flask
from flask import Flask, render_template, request, jsonify

app = Flask(__name__, static_url_path='/static')

knn = pickle.load( open( "knn.pkl", "rb" ) )

@app.route('/')
def welcome_page():
	return render_template('index.htm')

"""
Code below makes CURL and other webrequest able
to score new data
"""
@app.route('/score', methods=['GET', 'POST'])
def score():
	global knn

	data = request.get_json(force=True)
	print(data)
	
	prediction = knn.predict([np.array(data["values"])])
	return jsonify({"prediction": prediction.tolist()}) 

	
port = os.getenv('VCAP_APP_PORT', '5000')

if __name__ == "__main__":
	app.run(host='0.0.0.0', port=int(port), debug=True)
